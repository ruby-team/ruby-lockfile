ruby-lockfile (2.1.3-2) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Replace spaces in short license names with dashes.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Apply multi-arch hints.
    + ruby-lockfile: Add :any qualifier for ruby dependency.

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 05:55:24 +0530

ruby-lockfile (2.1.3-1) unstable; urgency=medium

  * Team upload
  * Imported Upstream version 2.1.3

 -- Cédric Boutillier <boutil@debian.org>  Sun, 27 Jul 2014 21:23:16 +0200

ruby-lockfile (2.1.0-4) unstable; urgency=low

  * Team upload.

  [ Cédric Boutillier ]
  * debian/control
    - remove obsolete DM-Upload-Allowed flag
    - use canonical URI in Vcs-* fields

  [ Jonas Genannt ]
  * d/control
    - removed transitional packages
    - bumped standards version to 3.9.5 (no changes needed)
  * added basic ruby-tests.rb files for testing on build time

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Fri, 24 Jan 2014 21:28:39 +0100

ruby-lockfile (2.1.0-3) unstable; urgency=low

  [ Jérémy Bobbio ]
  * Team upload.

  [ Per Andersson ]
  * Build depend on gem2deb >= 0.3.0 for better integration with rubygems.

 -- Jérémy Bobbio <lunar@debian.org>  Mon, 25 Mar 2013 11:26:49 +0100

ruby-lockfile (2.1.0-2) unstable; urgency=low

  * Correct binary ruby versions tag.
  * Relax conflicts with transitional package to breaks.
  * Escape hyphens used in rlock.1 man page.
  * Change section to ruby.
  * Add watch file.
  * Made long description a paragraph longer.

 -- Per Andersson <avtobiff@gmail.com>  Wed, 20 Jun 2012 15:50:43 +0200

ruby-lockfile (2.1.0-1) unstable; urgency=low

  * New maintainer. (Closes: #660502)
  * New upstream release.
  * Bumped standards version to 3.9.3 (no changes needed).
  * Changed to gem2deb packaging.
    - Rename source and binary package to ruby-lockfile.
    - Add liblockfile-ruby transitional package.
    - Build for all ruby versions. (Closes: #611821)
  * Remove unnecessary dirs file.
  * Use source format 3.0 (quilt).
  * Ship docs.
  * Update copyright to DEP-5 format.

 -- Per Andersson <avtobiff@gmail.com>  Wed, 25 Apr 2012 23:10:46 +0200

liblockfile-ruby (1.4.3-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/rules: add mandatory target binary-arch (Closes: #552666)

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 14 Nov 2009 12:35:00 +0100

liblockfile-ruby (1.4.3-2) unstable; urgency=low

  * Add man page for rlock

 -- Decklin Foster <decklin@red-bean.com>  Mon, 13 Oct 2008 20:15:46 -0400

liblockfile-ruby (1.4.3-1) unstable; urgency=low

  * Initial release (Closes: #488872)

 -- Decklin Foster <decklin@red-bean.com>  Thu, 26 Jun 2008 16:18:22 -0400
